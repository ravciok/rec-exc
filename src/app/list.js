
const createButton = Symbol('createButton');
const createList = Symbol('createList');
const createEvents = Symbol('createEvents');
const createAdditionalEvents = Symbol('createAdditionalEvents');

export default class List {
  constructor() {
    this.list = document.getElementById('table');
    this.template = '';
    this.toggle = document.querySelectorAll('[toggle]');
  }

  [createButton](num) {
    return `<li class="scale-transition">
              <div class="collapsible-header"><span><i class="material-icons">filter_drama</i>${num}. First</span><a class="waves-effect waves-light btn">delete</a></div>
              <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
            </li>`;
  }

  [createList]() {
    for (let i = 1; i <= 100; i++) {
      this.template += this[createButton](i);
    }

    this.list.innerHTML = this.template;
  }

  [createEvents]() {
    let arr = Array.from(this.list.children);
    let btns = this.deleteBtns;

    M.Collapsible.init(this.list);

    arr.forEach((value, key) => {

      if (key % 3 === 0) value.classList.add('enable');

      value.addEventListener('click', (ev) => {
        let el = ev.target.closest('li');

        if (key % 3 !== 0) ev.stopPropagation();
        if (ev.target.tagName === 'A') {
          el.classList.add('scale-out');
          setTimeout(() => { el.parentNode.removeChild(el); }, 500);
        }
      });
    });
  }

  [createAdditionalEvents]() {
    this.toggle.forEach(el => {
      el.addEventListener('click', (ev) => {
        let attr = ev.target.getAttribute('toggle');
        let col = document.querySelector(`aside.${attr}`);

        col.classList.contains('hidden') ? col.classList.remove('hidden') : col.classList.add('hidden');
      });
    });

    M.Sidenav.init(document.querySelectorAll('.sidenav'));
  }

  init() {
    this[createList]()
    this[createEvents]()
    this[createAdditionalEvents]()
  }
}
